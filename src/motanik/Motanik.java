package motanik;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import robocode.AdvancedRobot;
import robocode.BulletMissedEvent;
import robocode.HitByBulletEvent;
import robocode.RobotDeathEvent;
import robocode.Rules;
import robocode.ScannedRobotEvent;

public class Motanik extends AdvancedRobot  {
	
	List<Point> drawPoints = new LinkedList<>();
	Point myNextPos = new Point();
	// Cache the battlefield size
	private double battleFieldHeight;
	private double battleFieldWidth;
	boolean idle = false;
	// Sa salvati aici NEAPARAT rotatia sasiului (si mai tarziu a tunului)
	double stackedRotation = 0;
	// Dupa ce scanez un robot, salvez aici evenimentul. IDEAL, dupa asta va fi reactualizat in fiecare tick. ESTE :D
	ScannedRobotEvent e;
	// Evenimentul din tickul precedent.
	ScannedRobotEvent previousE;
	double previousEnemyHeading = 0;
	// Delta angular.
	double enemyHeadingChange = 0;
	
	public Tracker tracker;
	public GunControl gunControl;
	public Mover mover;

	boolean aproachTarget = true;
	Double enemyEnergy;
	double bulletPower;
	double stopTime = 0;
	boolean isShooting = false;
	
	double shotsFired = 0;
	double extraPower = 0;
	int shotsMissed = 0;
	
	private boolean enemyShooting(double energy, double speed, double bulletPower)
	{
	  if (enemyEnergy - energy == Rules.getWallHitDamage(speed))
	    return false;
	  if (enemyEnergy - energy == Rules.getBulletDamage(bulletPower))
	    return false;
	  return true;
	}
	
	Random rand = new Random();
	
	
	public void onRobotDeath(RobotDeathEvent event)
	{
		out.println("Accuracy: " + accuracy() +"|Hit/Total:" + (shotsFired-shotsMissed)+"/"+shotsFired + "| Extra power:" + extraPower);
		out.println("Strategy: " + strategies[0] + "/" + strategies[1] + "/" + strategies[2]);
	}
	
	
	public class Mover {
		
		int reversing = 0;
		final int reversTicks = 18;
		int maxStraightRot = 40;
		int maxStraight = 100;

		int timeToReverseRot = maxStraight;

		int rotation = 100;
		
		int moveDirection = 100;
		int timeToReverse = maxStraight;
		//boolean reverseNow = false; // DONE altfel: cand trage inamic reverse imediat?
		
		
		final int REVERSE = 1;
		final int STOP = 2;
		final int IGNORE = 3;
		int avoidStrategy = STOP; 
		
		public void step()
		{
			double maxRot = Utils.getMaxTurnRate(getVelocity());
			double currentHeading = getHeading();
			if (aproachTarget && e != null)
			{
				Point target = Utils.getPositionFromRadar(getX(), getY(), e.getBearing() + currentHeading, e.getDistance());
				Point eu = new Point(getX(), getY());
				double heading = Utils.getHeading(out, eu, target);
				double distToTarget = eu.distanceTo(target);// poate modificam unghiul in functie de distanta?
				double aproachAngle = 8;
				if (distToTarget < 50) aproachAngle = -8;
				else if (distToTarget < 100) aproachAngle = 0;
				else if (distToTarget > 500) aproachAngle = 25; // Optimizat de mana
				
				//double acc = accuracy();
				//out.println("Accuracy = " + acc);
				
				double targetHeading = (heading + (moveDirection>0?(90-aproachAngle):(90+aproachAngle))) %360;				
				double dist = Utils.angleDistance(targetHeading, currentHeading);
				
				double distMaxAbs = Math.min(Math.abs(dist), maxRot);
	            if(dist>0)
	            {
	                dist = distMaxAbs;
	            }
	            else
	            {
	                dist = -distMaxAbs;
	            }
	            maxRot = dist;
			}
			else
			{
				if (timeToReverseRot <= 0)
	            {
	            	timeToReverseRot = maxStraightRot/2 + rand.nextInt(maxStraightRot);
	            	rotation = 100 * (rand.nextInt(3) - 1);
	                out.println("rotation " + rotation);
	            }
	            else
	            {
	            	timeToReverseRot--;
	            }
				maxRot = Math.signum(rotation) * maxRot;
			}
            
            
            if (reversing == 0)
            {
	            double minBorderDistance = Utils.distanceToBorders(myNextPos, battleFieldWidth, battleFieldHeight);
	            if (minBorderDistance < 100 || /*timeToReverse <= 0 || reverseNow ||*/ (isShooting && avoidStrategy == REVERSE))
	            {
//	            	out.println("Reverse");
	            	reversing = reversTicks;
	            	moveDirection = - moveDirection;
	            	timeToReverse = maxStraight/4 + rand.nextInt(maxStraight);
	            	//reverseNow = false;
	            }
	            else
	            {
	            	timeToReverse--;
	            }
            }
            else
            {
            	reversing--;
            	timeToReverse--;
            }
//	            out.println(dist);
            setTurnLeft(maxRot);
            stackedRotation = maxRot;
            if (avoidStrategy == STOP && stopTime > 0)
            {
//            	out.println("Stop " + stopTime);
            	setAhead(0);
            	stopTime--;
            	timeToReverse++;
            	timeToReverseRot++;
            }
            else
            {
//                out.println("Move " + moveDirection);
            	setAhead(moveDirection);
            }
		}
	}
	
	public void onPaint(Graphics2D g) {
		g.setColor(Color.red);
		for (Point point : drawPoints)
		{
			g.fillArc((int)Math.round(point.x-3), (int)Math.round(point.y-3), 7, 7, 0, 360);
		}
		drawPoints.clear();
	}
	
	public class EnemyFire
	{
		int strategyUsed;
		int predictedimpactTime;
	}
	
	
	List<EnemyFire> enemyFires = new LinkedList<>();
	
	public void onHitByBullet(HitByBulletEvent eventBullet)
	{
		// Asta pare sa fie un efort inutil vs botii default...Cel putin din statistici.
		// Poate ajuta vs playeri, tare as fi vrut sa fiu acolo sa vad cum joaca :( competitia :(
		// Oricum rau nu pare sa faca...
		Iterator<EnemyFire> iterator = enemyFires.iterator();
		long time = eventBullet == null ? getTime() : eventBullet.getTime();
		EnemyFire bestMatch = null;
		double dist =15;
		
		while(iterator.hasNext())
		{
			EnemyFire event = iterator.next();
			if (event.predictedimpactTime < (time - 15))
			{
				// This strategy worked!
				strategies[event.strategyUsed-1]++;
				iterator.remove();
			}
			else if (event.predictedimpactTime > (time + 5))
			{
				break;
			}
			else if (eventBullet != null)
			{
				double cd = Math.abs(event.predictedimpactTime - time);
				if (cd < dist)
				{
					bestMatch = event;
					dist = cd;
				}
			}
		}
		if (bestMatch != null)
		{
			// This failed!
			enemyFires.remove(bestMatch);
			strategies[bestMatch.strategyUsed-1] --;
			if (strategies[bestMatch.strategyUsed-1] <1 ) strategies[bestMatch.strategyUsed-1]=1;
		}
	}	
	
	int[] strategies = new int[] {1,1,1}; 
		
	public class Tracker {
		
		private Double heading = null;
		boolean rotateLeft = true;
		
		public void step()
		{
			if (heading == null)
			{
				if (rotateLeft)
					setTurnRadarLeft(45);
				else
					setTurnRadarRight(45);
				idle = false;
			}
			else
			{
				double rotation = getRadarHeading() - heading - stackedRotation;
				if (rotation > 180) rotation -= 360;
				else if (rotation < -180) rotation += 360;
				setTurnRadarLeft(rotation);
				rotateLeft = rotation < 0; 
				heading = null;
				idle = false;
				//scan();
			}
		}

		public void onScannedRobot(ScannedRobotEvent e) {
			// NOTE: e o problema aici, cumva un tick detecteaza, unul nu, merge din doi in doi. Nu stiu de ce!?
			// Note2: S-a rezolvat (desi nu stiu de ce) Acum e in fiecare tick.
//			out.println(e.getTime() + " Enemy detected at " + e.getDistance() + " / " + e.getHeading() + "/"+ e.getVelocity());
			//out.println("Radar at " + getRadarHeading() +"/"+ getGunHeading()+"/"+ getHeading());
			double targetHeading = Utils.normalizeAngle(e.getBearing() + getHeading());
			Point enemyPos = Utils.getPositionFromRadar(getX(), getY(), targetHeading, e.getDistance());
			Point position = Utils.getPosition(1, enemyPos.x, enemyPos.y, e.getHeading(), e.getVelocity(), 0, 0, battleFieldWidth, battleFieldHeight, null);
//			drawPoints.add(position);
			
			if (enemyEnergy == null)
			{
			  enemyEnergy = e.getEnergy();
			}
			if (enemyEnergy > e.getEnergy())
			{
			  isShooting = enemyShooting(e.getEnergy(), e.getVelocity(), bulletPower) ;
			  if (isShooting)
			  {
				  double delta = enemyEnergy - e.getEnergy();
				  double enemyBulletSpeed = 20 - delta * 3;
				  int predictedImpact = (int) (getTime() + enemyPos.distanceTo(myNextPos)/enemyBulletSpeed);
				  
				  double bulletSpeed = 20 - bulletPower * 3;
				  stopTime = (e.getDistance() / bulletSpeed) / 2;
				  stopTime = Math.min(10, stopTime);
				  int total = strategies[0]+strategies[1]+strategies[2];
				  int choice = rand.nextInt(total);
				  
				  if (choice < strategies[0])
				  {
					  mover.avoidStrategy = mover.REVERSE;
//					  out.println("Strategy:REVERSE");
				  }
				  else if (choice < strategies[0]+strategies[1])
				  {
					  mover.timeToReverse =mover. maxStraight/2 + rand.nextInt(mover.maxStraight);
					  mover.avoidStrategy = mover.STOP;
//					  out.println("Strategy:STOP");
				  }
				  else
				  {
//					  out.println("Strategy:IGNORE");
					  mover.avoidStrategy = mover.IGNORE;
					  isShooting = false;
				  }
				  EnemyFire event = new EnemyFire();
				  event.predictedimpactTime = predictedImpact;
				  event.strategyUsed = mover.avoidStrategy;
				  enemyFires.add(event);
			  }
			  enemyEnergy = e.getEnergy();
			} else
			{
			  isShooting = false;
			  enemyEnergy = e.getEnergy();
			}
			
			heading = Utils.getHeading(out, myNextPos, position);
			//out.println(heading);
		}
	}
	
	public void onBulletMissed(BulletMissedEvent event)
	{
		shotsMissed++;
	}
	
	public double accuracy() 
	{
		if (shotsFired == 0) return 1;
		return 1 - (shotsMissed / shotsFired);
	}
	
	public class GunControl {
		
		private Double heading = null;
		public boolean POATE_TRAGE = true;
		public double power = 1;
		public double defaultMinimalPower = 1;
		
		public void step()
		{
			if (heading == null)
			{
				setTurnGunLeft(0);
			}
			else
			{
				double rotation = getGunHeading() - heading - stackedRotation;
				if (rotation > 180) rotation -= 360;
				else if (rotation < -180) rotation += 360;
				if (rotation > 20) rotation = 20;
				else if (rotation < -20) rotation = -20;
				setTurnGunLeft(rotation);
				heading = null;
				stackedRotation += rotation;

				if ((POATE_TRAGE // Debug: pus off sa nu ne buleasca robotul partener 
						|| fireOnce) // fire once controlat din mouse, sa vedem totusi cum trage. 
						/** Adaugare conditie mai de doamne ajuta, nu sa trag cat de repede pot **/)
				{
					if (getGunHeat() > 0)
					{
						// Do not "think" we fired, do not schedule stupid fire later
					}
					else
					{
						shotsFired++;
						extraPower += (power - 1);
						setFire(power);
					}
//					out.println("FIRE!");
					fireOnce = false;
				}
			}
		}

		public void onScannedRobot(ScannedRobotEvent e) {
			double targetHeading = Utils.normalizeAngle(e.getBearing() + getHeading());
			Point enemyPos = Utils.getPositionFromRadar(getX(), getY(), targetHeading, e.getDistance());
			Point position, maxFwd, maxBack;
			int turnsToHit2;
			double distance;
			power = defaultMinimalPower;
			do
			{
				double bulletSpeed = 20 - power * 3;
				distance = myNextPos.distanceTo(enemyPos);
				turnsToHit2 = (int)Math.ceil(distance / bulletSpeed);
//				out.println("turns to hit:" + turnsToHit2);
				position = simulateIntercept(distance, turnsToHit2, enemyPos, bulletSpeed, 0);
				maxFwd = simulateIntercept(distance, turnsToHit2, enemyPos, bulletSpeed, e.getVelocity()>0 ? 1:2);
				maxBack = simulateIntercept(distance, turnsToHit2, enemyPos, bulletSpeed, e.getVelocity()>0 ? -2:-1);
					
				drawPoints.add(position);
				drawPoints.add(maxFwd);
				drawPoints.add(maxBack);
				if (turnsToHit2 < accuracy()*25)
				{
					power+=0.2;
//					out.println("Power boost!" + power);
				}
				else
				{
					break;
				}
			}while(power <= 3);
			bulletPower = power; // Asta se schimba, decuplam mai tarziu
			heading = Utils.getHeading(out, myNextPos, position);
		}
		
		private Point simulateIntercept(double distance, int turnsToHit2, Point enemyPos, double bulletSpeed, int acceleration) 
		{
			int turnsToHit1;
			int maxRuns = 10;
			Point position;
			do
			{
				if (acceleration == 0) drawPoints.clear();
				turnsToHit1 = turnsToHit2;
				// Simleaza miscarea cu acceleratie constanta (dar considera limita de viteza etc).
				// TODO: cum sa gasesc si viteza unghiulara (turning speed)?
				position = Utils.getPosition(turnsToHit1, enemyPos.x, enemyPos.y, e.getHeading(), e.getVelocity(), acceleration, enemyHeadingChange, battleFieldWidth, battleFieldHeight, acceleration == 0 ? drawPoints : null);
				// Now compute distance to predicted interception
				distance = myNextPos.distanceTo(position);
				turnsToHit2 = (int)Math.ceil(distance / bulletSpeed);				
			} while(turnsToHit1 != turnsToHit2 && maxRuns-->0); // TODO lista de pozitii generata pana cand glontele "trece" de pozitia de inteceptare
			//out.println("Prediction after " + maxRuns + " impact in " + turnsToHit1); // De multe ori asta merge in maxRuns ca face de exe 4->5 5->4 
			return position;
		}
	}
	
	public void run() {
		out.println("Robot start");
		tracker = new Tracker();
		mover = new Mover();
		gunControl = new GunControl();
		battleFieldHeight = getBattleFieldHeight();
		battleFieldWidth = getBattleFieldWidth();
		while(true)
		{
			onHitByBullet(null);
			myNextPos.x = getX();
            myNextPos.y = getY();
            idle = true;
            mover.step();
            gunControl.step();
			tracker.step();
			scan();
			if (idle)
			{
				this.doNothing();
			}
		}
	}
	
	boolean fireOnce = false;
	public void onMouseClicked(MouseEvent e)
	{
		fireOnce = true;
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		idle = false;
		previousE = this.e;
		this.e = e;
		double currentEnemyHeading = e.getHeading();
		if (previousE != null)
		{
			this.enemyHeadingChange = Utils.angleDistance(currentEnemyHeading, previousEnemyHeading);
			
			//out.println("EHC:"+enemyHeadingChange + " 1="+previousEnemyHeading+";2="+currentEnemyHeading);
			
		}
		previousEnemyHeading = currentEnemyHeading;
		tracker.onScannedRobot(e);
		gunControl.onScannedRobot(e);
	}
	
}
