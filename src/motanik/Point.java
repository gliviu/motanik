package motanik;

public class Point {

	@Override
	public String toString() {
		return "Point [x=" + Math.round(x) + ", y=" + Math.round(y) + "]";
	}

	public Point()
	{
		
	}
	
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double x,y;

	public double distanceTo(Point enemyPos) {
		double dx = enemyPos.x - x;
		double dy = enemyPos.y - y;
		return Math.sqrt(dx*dx + dy*dy);
	}
	
}
