package motanik;

import java.io.PrintStream;
import java.util.List;

public class Utils {

	
	/**
	 * Returneaza un unghi normalizat (intre 0 si 360) eliminant valorile negative sau cele peste 360.
	 * @param angle
	 * @return
	 */
	public static double normalizeAngle(double angle) {
		while(angle < 0) angle += 360;
		while(angle >= 360) angle -= 360;
		return angle;
	}
	
	/**
	 * Calculeaza pozitia unui punct pe baza unei distante fata de un punct dat si a unui unghi absolut
	 * @param x Pozitia x
	 * @param y Pozitia y
	 * @param angle Unghiul absolut (heading)
	 * @param distance Distanta
	 * @return Punctul rezultat
	 */
	public static Point getPositionFromRadar(double x, double y, double angle, double distance) {
		angle=normalizeAngle(angle);
		
		y += distance * Math.cos(Math.toRadians(angle));
		x += Math.sin(Math.toRadians(angle)) * distance;
		return new Point(x,y);
	}
	
	/**
	 * Calculeaza pozitia viitoare a unui tank care se deplaseaza fara schimbarea acceleratiei si cu schimbarea constanta a directiei pentru un anumit numar de pasi
	 * @param steps Cati pasi sa simulam
	 * @param x Pozitia curenta x
	 * @param y Pozitia curenta y
	 * @param heading Unghiul de deplasare ca valoare absoluta (coordonate robocode, 0 = sus, 270=stanga
	 * @param velocity Viteza INITIALA ca valoare absoluta
	 * @param acceleration Aceleratia. Nu are sens sa fie altceva decat 2 (acceleram in fata), 4 (frana) sau 0(viteza constanta, mai precis, sta pe loc).
	 * @param angularRotationLeft Viteza de rotire, spre stanga (valori negative inseamna rotire spre dreapta)
	 * @param battleFieldWidth Dimensiunea arenei pe x
	 * @param battleFieldHeight Dimensiunea arenei pe y
	 * @return punctul resultat (coordonate)
	 */
	public static Point getPosition(int steps, double x, double y, double heading, double velocity, int acceleration, double angularRotationLeft,
			double battleFieldWidth, double battleFieldHeight, List<Point> toDraw) {
		for(int i=0;i<steps;i++)
		{
			heading -= angularRotationLeft;
			if (heading < 0)
			{
				heading += 360;
			}
			else if (heading >= 360)
			{
				heading -= 360;
			}
			// update velocity
			if (acceleration != 0 || velocity != 0)
			{
				double oldVel = velocity;
				velocity += acceleration;
				if (velocity > 8) {velocity = 8;acceleration=0;}
				else if (velocity < -8) {velocity = -8;acceleration=0;}
				if ((oldVel > 0 && velocity < 0) || (oldVel < 0 && velocity > 0)) {velocity = 0;acceleration = acceleration/2;} // tank switches from breaking to acceleration in opposite direction
				
				y += Math.cos(Math.toRadians(heading)) * velocity;
				x += Math.sin(Math.toRadians(heading)) * velocity;
				if (x < 0) 
				{
					x = 0;
					acceleration = 0;
					velocity = 0;
				}
				else if (x >= battleFieldWidth)
				{
					x = battleFieldWidth-1;
					acceleration = 0;
					velocity = 0;
				}
				if (y < 0)
				{
					y = 0;
					acceleration = 0;
					velocity = 0;
				}
				else if (y >= battleFieldHeight)
				{
					y = battleFieldHeight -1;
					acceleration = 0;
					velocity = 0;
				}
				if (toDraw != null) toDraw.add(new Point(x,y));
			}
		}
		return new Point(x,y);
		
	}
	
	/**
	 * Calculeaza unghiul absolut (heading)
	 * @param out
	 * @param observer
	 * @param target
	 * @return
	 */
	public static double getHeading(PrintStream out, Point observer, Point target) {
		double dx = target.x - observer.x;
		double dy = target.y - observer.y;
		
		if (Math.abs(dy) < 0.005f) // cannot both be infinitesimal, bots would crash? 
		{
			if (dx > 0)
			{
				return 270;
			}
			else
			{
				return 90;
			}
		}
		
		double atan = Math.atan(dx/dy);
		double angle = Math.toDegrees(atan);
		if (dy < 0) angle += 180;
		//out.println(angle);
		
		while (angle > 360) angle-=360;
		return angle;
	}
	
	/**
	 * Calculeaza rata maxima de rotire la o anumita viteza
	 * @param velocity
	 * @return
	 */
	public static double getMaxTurnRate(double velocity)
	{
		return 10 - 0.75 * Math.abs(velocity);
	}
	
	/**
	 * Calculeaza distanta unghiulara intre doua headinguri
	 * @param from
	 * @param to
	 * @return relative rotation TOWARDS LEFT required to get from from to to. Negative means shortest way is turning right.
	 */
	public static double angleDistance(double from, double to) {
		double distanceRight = (360+from-to) % 360;
		double distanceLeft = (360+to-from)%360;
		return distanceLeft < distanceRight ? distanceLeft : -distanceRight;
	}
	
	public static double distanceToBorders(Point myNextPos, double battleFieldWidth, double battleFieldHeight) {
		return Math.min(Math.min(myNextPos.x, battleFieldWidth - myNextPos.x),
				Math.min(myNextPos.y, battleFieldHeight - myNextPos.y));
	}
}
